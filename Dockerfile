FROM ubuntu:16.10

RUN apt-get update && apt-get install -y curl \
    && curl https://winswitch.org/gpg.asc | apt-key add - \
    && echo "deb http://winswitch.org/ xenial main" > /etc/apt/sources.list.d/winswitch.list \
    && apt-get update \
    && DEBIAN_FRONTEND=noninteractive apt-get install -y openssh-server \
    		x11-apps xterm language-pack-en-base \
    		xserver-xephyr xpra \
    		xserver-xorg-video-dummy \
    		dbus-x11 python-dbus python-avahi python-netifaces \
    		python-xxhash \
    		pulseaudio \
    && apt-get autoremove -y && apt-get clean && rm -rf /var/lib/apt/lists/* /var/tmp/*

ADD ./xorg.conf /etc/xpra/xorg.conf
ADD ./entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh

ENTRYPOINT /entrypoint.sh

EXPOSE 8080
