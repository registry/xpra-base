#!/bin/bash

service cups start
service dbus start

exec xpra start \
	--pulseaudio=no \
	--mdns=no \
	--mmap=yes \
	--bind-tcp=0.0.0.0:8080 \
	--html=on \
	--no-daemon \
	--exit-with-children \
	--dpi=100 \
	--start-child="$@" \
	--xvfb='/usr/bin/Xorg -dpi 100 -noreset -nolisten tcp +extension GLX +extension RANDR +extension RENDER -logfile /tmp/Xorg.log -config /etc/xpra/xorg.conf'